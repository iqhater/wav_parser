package wav

import (
	"bytes"
	"testing"
)

func TestUint32ToBytes(t *testing.T) {

	expected := []byte{125, 0, 0, 0}
	testData := uint32(125)

	if bytes.Compare(expected, uint32ToBytes(testData, uint(4))) != 0 {
		t.Error("Byte slices doesn't equal!")
	}
}
