package wav

// AudioData represents wav audio format.
type AudioData struct {
	header         Header
	OneChannelData []uint32 // if wav have one or two audio channel
	TwoChannelData []uint32 // if wav have two audio channels
}

// GetHeader returns copy of wav header.
func (r *AudioData) GetHeader() Header {
	return r.header
}

// String return string representation of audio data
func (r *AudioData) String() string {
	return r.header.String()
}
