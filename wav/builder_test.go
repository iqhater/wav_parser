package wav

import (
	"math"
	"reflect"
	"testing"
)

func TestBuilderConstructor(t *testing.T) {
	inputHeader := Header{PCMFormat, 2, SRate44K, 0, 0, BPS8}

	if BuilderConstructor() == nil {
		t.Error("nil builder pointer")
		return
	}

	if inputHeader != BuilderConstructor().header {
		t.Error("Wrong header builder!")
	}
}
func TestBuild(t *testing.T) {

	inputByteRate := uint32(88200)
	inputBlockAlign := uint16(2)

	header := Header{PCMFormat, 2, SRate44K, inputByteRate, inputBlockAlign, BPS8}
	builder := AudioDataBuilder{header}

	if !reflect.DeepEqual(AudioData{header, nil, nil}, *builder.Build()) {
		t.Error("Wrong FileWav header!")
	}
}
func TestSetChannelsWrongData(t *testing.T) {

	builder := AudioDataBuilder{}
	result1 := builder.SetChannels(0)
	result2 := builder.SetChannels(3)

	if result1 != nil || result2 != nil {
		t.Error("Wrong channels number!")
	}
}
func TestSetChannels(t *testing.T) {

	builder := AudioDataBuilder{}
	result1 := builder.SetChannels(1).header
	result2 := builder.SetChannels(2).header

	if result1.NumChannels != 1 || result2.NumChannels != 2 {
		t.Error("Wrong channels number!")
	}
}
func TestSetSampleRate(t *testing.T) {

	inputRates := []uint32{SRate8K, SRate11K, SRate16K, SRate22K, SRate32K, SRate44K, SRate48K, SRate88K, SRate96K, SRate176K, SRate196K}
	builder := AudioDataBuilder{}

	for _, v := range inputRates {

		if builder.SetSampleRate(v) == nil {
			t.Error("Wrong samples rate!")
		}

		if builder.header.SampleRate != v {
			t.Error("Wrong sample rate header!")
		}
	}
}
func TestSetSampleRateWrongData(t *testing.T) {

	builder := AudioDataBuilder{}

	if builder.SetSampleRate(0) != nil {
		t.Error("Sample rate is not nil!")
	}
}
func TestSetBPS(t *testing.T) {

	inputBits := []uint16{BPS8, BPS16, BPS24, BPS32}
	builder := AudioDataBuilder{}

	for _, v := range inputBits {

		if builder.SetBitsPerSample(v) == nil {
			t.Error("Wrong bits per samples!")
		}

		if builder.header.BitsPerSample != v {
			t.Error("Wrong bits per sample header!")
		}
	}
}
func TestSetBPSWrongData(t *testing.T) {

	builder := AudioDataBuilder{}

	if builder.SetBitsPerSample(0) != nil {
		t.Error("Bits per sample is not nil!")
	}
}
func TestSetAudioFormat(t *testing.T) {

	inputFormats := []uint16{PCMFormat, FloatFormat}
	builder := AudioDataBuilder{}

	for _, v := range inputFormats {

		if builder.SetAudioFormat(v) == nil {
			t.Error("Unsupported audio format!")
		}

		if builder.header.AudioFormat != v {
			t.Error("Wrong audio format header!")
		}
	}
}
func TestSetAudioFormatWrongData(t *testing.T) {

	builder := AudioDataBuilder{}

	if builder.SetAudioFormat(math.MaxUint16) != nil {
		t.Error("Audio format is not nil!")
	}
}
