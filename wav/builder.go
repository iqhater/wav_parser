package wav

// AudioDataBuilder is a builder pattern. It's a safety generate different headers for wav files.
// We strongly suggest use builder to create all audioData struct.
type AudioDataBuilder struct {
	header Header
}

// BuilderConstructor return new builder constructor with default header.
func BuilderConstructor() *AudioDataBuilder {

	header := Header{PCMFormat, 2, SRate44K, 0, 0, BPS8}
	builder := AudioDataBuilder{header}
	return &builder
}

// SetChannels check and return number of channels.
func (b *AudioDataBuilder) SetChannels(num uint16) *AudioDataBuilder {

	if num > 2 || num == 0 {
		return nil
	}
	b.header.NumChannels = num
	return b
}

// SetSampleRate check and return sample rate of wav file
func (b *AudioDataBuilder) SetSampleRate(rate uint32) *AudioDataBuilder {

	switch rate {
	case SRate8K:
	case SRate11K:
	case SRate16K:
	case SRate22K:
	case SRate32K:
	case SRate44K:
	case SRate48K:
	case SRate88K:
	case SRate96K:
	case SRate176K:
	case SRate196K:
	default:
		return nil
	}

	b.header.SampleRate = rate
	return b
}

// SetBitsPerSample check and return bits per sample of wav file
func (b *AudioDataBuilder) SetBitsPerSample(bps uint16) *AudioDataBuilder {

	switch bps {
	case BPS8:
	case BPS16:
	case BPS24:
	case BPS32:
	default:
		return nil
	}

	b.header.BitsPerSample = bps
	return b
}

// SetAudioFormat function set and check audio format 1 or 3
func (b *AudioDataBuilder) SetAudioFormat(format uint16) *AudioDataBuilder {

	switch format {
	case PCMFormat:
	case FloatFormat:
	default:
		return nil
	}

	b.header.AudioFormat = format
	return b
}

// Build complete building a wav file
func (b *AudioDataBuilder) Build() *AudioData {

	b.header.ByteRate = b.header.SampleRate * uint32(b.header.NumChannels*b.header.BitsPerSample) / uint32(byteSize)
	b.header.BlockAlign = b.header.NumChannels * b.header.BitsPerSample / uint16(byteSize)
	return &AudioData{b.header, nil, nil}
}
