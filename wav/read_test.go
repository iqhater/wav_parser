package wav

import (
	"bytes"
	"reflect"
	"testing"
)

func TestReadWav(t *testing.T) {

	ad := &AudioData{
		header:         Header{AudioFormat: 1, NumChannels: 2, SampleRate: 44100, ByteRate: 176400, BlockAlign: 4, BitsPerSample: 16},
		OneChannelData: nil,
		TwoChannelData: nil,
	}

	expected, _ := ReadWav("../A2.wav")

	if !reflect.DeepEqual(ad.header, expected.header) {
		t.Error("Struct's have different values! Must be equal.")
	}
}

func TestFindChunk(t *testing.T) {

	// Arrange
	testSize := uint32(55)
	testID := []byte("ABCD")

	testData := append([]byte("sdfsdf"), testID...)
	testData = append(testData, uint32ToBytes(testSize, 4)...)

	// Act
	position, size := findChunk(testID, &testData)

	// Assert
	realPosition := int32(bytes.Index(testData, testID))
	if position != realPosition+8 || size != int32(testSize) {
		t.Error("Failed data!")
	}
}
func TestFindChunkNoChunk(t *testing.T) {

	// Act
	position, size := findChunk([]byte{'0'}, &[]byte{'Q'})

	// Assert
	if position != -1 || size != 0 {
		t.Error("Failed data!")
	}
}
func TestParseFmt(t *testing.T) {

	// Arrange
	builder := BuilderConstructor()
	testHeader := builder.SetAudioFormat(PCMFormat).SetChannels(2).SetSampleRate(SRate48K).SetBitsPerSample(BPS24).Build().header

	headerArray := createHeader(testHeader)

	// Act
	resultHeader := parseFmt(headerArray).header

	// Assert
	if !reflect.DeepEqual(testHeader, resultHeader) {
		t.Error("parseFmt return wrong data!")
	}
}

func TestParseFmtWrongFmtChunks(t *testing.T) {

	tests := []Header{
		{NumChannels: 1, AudioFormat: 4},
		{NumChannels: 0, AudioFormat: 1},
		{NumChannels: 1, SampleRate: 0, AudioFormat: 1},
		{NumChannels: 1, BitsPerSample: 0, SampleRate: 8000, AudioFormat: 1},
	}

	for _, tt := range tests {

		t.Run("test wrong chunks", func(t *testing.T) {
			if got := parseFmt(createHeader(tt)); got != nil {
				t.Errorf("parseFmt() = %v, want %v", got, tt)
			}
		})

	}
}

func createHeader(header Header) []byte {

	headerArray := append(uint32ToBytes(uint32(header.AudioFormat), 2), uint32ToBytes(uint32(header.NumChannels), 2)...)
	headerArray = append(headerArray, uint32ToBytes(header.SampleRate, 4)...)
	headerArray = append(headerArray, uint32ToBytes(header.ByteRate, 4)...)
	headerArray = append(headerArray, uint32ToBytes(uint32(header.BlockAlign), 2)...)
	headerArray = append(headerArray, uint32ToBytes(uint32(header.BitsPerSample), 2)...)

	return headerArray
}

/* func TestParseData(t *testing.T) {

	fw := &AudioData{}
	data := make([]byte, 200)

	for i := 0; i < len(data); i++ {
		data[i] = 2
	}

	parseData(data, fw)
} */

func TestBytesToUint32(t *testing.T) {

	// Arrange
	byteSlice1 := []byte("A")
	byteSlice2 := []byte("AB")
	byteSlice3 := []byte("ABC")
	byteSlice4 := []byte("ABCD")

	out1 := uint32(byteSlice1[0])
	out2 := uint32(byteSlice2[0]) | uint32(byteSlice2[1])<<8
	out3 := uint32(byteSlice3[0]) | uint32(byteSlice3[1])<<8 | uint32(byteSlice3[2])<<16
	out4 := uint32(byteSlice4[0]) | uint32(byteSlice4[1])<<8 | uint32(byteSlice4[2])<<16 | uint32(byteSlice4[3])<<24

	// Act
	result1 := bytesToUint32(byteSlice1)
	result2 := bytesToUint32(byteSlice2)
	result3 := bytesToUint32(byteSlice3)
	result4 := bytesToUint32(byteSlice4)

	// Assert
	if out1 != result1 || out2 != result2 || out3 != result3 || out4 != result4 {
		t.Error("bytesToUint32 is not equal results!")
	}
}
func TestBytesToUint32WrongData(t *testing.T) {

	// Arrange
	byteSlice := []byte{}

	// Act
	result := bytesToUint32(byteSlice)

	// Assert
	if result != 0 {
		t.Error("bytesToUint32 doesn't return zero!")
	}
}
