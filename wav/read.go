package wav

import (
	"bytes"
	"encoding/binary"
	"io/ioutil"
)

// ParseError check all errors
type ParseError struct {
	errorString string
}

// Error implements standart error interface
func (e *ParseError) Error() string {
	return "Parse file error: " + e.errorString
}

//TODO: MAKE TEST ReadWav

// ReadWav function read the wav file
func ReadWav(fileName string) (*AudioData, error) {

	// Read file
	data, err := ioutil.ReadFile(fileName)
	if err != nil {
		return nil, &ParseError{err.Error()}
	}

	// Check RIFF
	riffPos, _ := findChunk(getRiffID(), &data)

	if riffPos == -1 {
		return nil, &ParseError{"RIFF chunk not found in file!"}
	}

	// Parse fmt
	fmtPos, fmtSize := findChunk(getFmtID(), &data)

	if fmtPos == -1 {
		return nil, &ParseError{"fmt chunk not found in file!"}
	}

	if fmtSize != int32(fmtHeaderSize) {
		return nil, &ParseError{"fmt chunk corrupted!"}
	}

	fileWav := parseFmt(data[fmtPos : fmtPos+fmtSize])
	if fileWav == nil {
		return nil, &ParseError{"Unsupported fmt header!"}
	}

	// Parse data
	dataPos, dataSize := findChunk(getDataID(), &data)
	return parseData(data[dataPos:dataPos+dataSize], fileWav), nil
}

//TODO: REMOVE MAGIC CONSTANTS findChunk

// findChunk function find chunk id and return chunk's position and size
func findChunk(id []byte, fileData *[]byte) (int32, int32) {

	position := int32(bytes.Index(*fileData, id))

	if position < 0 {
		return -1, 0
	}

	size := binary.LittleEndian.Uint32((*fileData)[position+4 : position+8])
	return position + 8, int32(size)
}

//TODO: REMOVE MAGIC CONSTANTS IF POSSIBLE

// parseFmt function gets fmt chunks from header array and return them to Header struct.
func parseFmt(headerArray []byte) *AudioData {

	builder := BuilderConstructor()

	//set audio format
	audioFormat := binary.LittleEndian.Uint16(headerArray[0:2])
	if builder.SetAudioFormat(audioFormat) == nil {
		return nil
	}

	//set num off channels
	numChannels := binary.LittleEndian.Uint16(headerArray[2:4])
	if builder.SetChannels(numChannels) == nil {
		return nil
	}

	//set sample rate
	sampleRate := binary.LittleEndian.Uint32(headerArray[4:8])
	if builder.SetSampleRate(sampleRate) == nil {
		return nil
	}

	//get calculated data from file
	byteRate := binary.LittleEndian.Uint32(headerArray[8:12])
	blockAlign := binary.LittleEndian.Uint16(headerArray[12:14])

	//set bitsPerSample
	bitsPerSample := binary.LittleEndian.Uint16(headerArray[14:16])
	if builder.SetBitsPerSample(bitsPerSample) == nil {
		return nil
	}

	fileWav := builder.Build()

	//check calculated data
	if fileWav.header.ByteRate != byteRate || fileWav.header.BlockAlign != blockAlign {
		return nil
	}

	return fileWav
}

//TODO: MAKE TEST FOR parseData
//TODO: MAKE PARALLEL CHANNEL READ

// parseData function write data in channels or one channel
func parseData(data []byte, fileWav *AudioData) *AudioData {

	fh := fileWav.GetHeader()

	// Create channels arrays
	channelSamplesNum := int32(len(data)*byteSize) / int32(fh.BitsPerSample*fh.NumChannels)
	firstChanSlice := make([]uint32, channelSamplesNum)

	var secondChanSlice []uint32
	if fh.NumChannels == 2 {

		secondChanSlice = make([]uint32, channelSamplesNum)
	}

	// Start analyze data
	bytesPerSample := int(fh.BitsPerSample) / byteSize
	totalSamples := int(channelSamplesNum * int32(fh.NumChannels))

	for i := 0; i < totalSamples; i++ {

		dataIndex := i * bytesPerSample
		subArray := data[dataIndex : dataIndex+bytesPerSample]

		// Parse bytes to int
		number := bytesToUint32(subArray)

		// Split data to channels
		if i%int(fh.NumChannels) == 0 {
			firstChanSlice[i/int(fh.NumChannels)] = number
		} else {
			secondChanSlice[i/2] = number
		}
	}

	fileWav.OneChannelData = firstChanSlice
	fileWav.TwoChannelData = secondChanSlice

	return fileWav
}

// bytesToUint32 function return a wav number bits
func bytesToUint32(subSlice []byte) uint32 {

	switch len(subSlice) {
	case 1:
		return uint32(subSlice[0])
	case 2:
		return uint32(subSlice[0]) | uint32(subSlice[1])<<8
	case 3:
		return uint32(subSlice[0]) | uint32(subSlice[1])<<8 | uint32(subSlice[2])<<16
	case 4:
		return uint32(subSlice[0]) | uint32(subSlice[1])<<8 | uint32(subSlice[2])<<16 | uint32(subSlice[3])<<24
	}
	return 0
}
