package wav

import "fmt"

// Audio format constants
const (
	PCMFormat   uint16 = 1
	FloatFormat uint16 = 3
)

// All available bits per sample for wav file
const (
	BPS8 uint16 = (iota + 1) * uint16(byteSize)
	BPS16
	BPS24
	BPS32
)

// All available samples rate for wav file
const (
	SRate8K   uint32 = 8000
	SRate11K  uint32 = 11025
	SRate16K  uint32 = 16000
	SRate22K  uint32 = 22050
	SRate32K  uint32 = 32000
	SRate44K  uint32 = 44100
	SRate48K  uint32 = 48000
	SRate88K  uint32 = 88200
	SRate96K  uint32 = 96000
	SRate176K uint32 = 176400
	SRate196K uint32 = 192000
)

// Header struct contain a wav header variables from 20 bytes to 36 bytes
type Header struct {
	AudioFormat   uint16 // 2 bytes
	NumChannels   uint16 // 2 bytes
	SampleRate    uint32 // 4 bytes
	ByteRate      uint32 // 4 bytes
	BlockAlign    uint16 // 2 bytes
	BitsPerSample uint16 // 2 bytes
}

// String return string representation of wav header
func (h Header) String() string {
	return fmt.Sprintf("{AudioFormat:%v NumChannels:%v SampleRate:%v ByteRate:%v BlockAlign:%v BitsPerSample:%v}\n", h.AudioFormat, h.NumChannels, h.SampleRate, h.ByteRate, h.BlockAlign, h.BitsPerSample)
}
