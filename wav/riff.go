package wav

const (
	fmtHeaderSize uint32 = 16
	int16Size     uint   = 2
	int8Size      uint   = 1
	int32Size     uint   = 4
	byteSize      int    = 8
)

func getWaveID() []byte {
	return []byte("WAVE")
}

func getFmtID() []byte {
	return []byte("fmt ")
}

func getRiffID() []byte {
	return []byte("RIFF")
}

func getDataID() []byte {
	return []byte("data")
}
