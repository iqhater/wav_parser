package wav

import (
	"bufio"
	"encoding/binary"
	"os"
)

const riffHeaderByteSize = fmtHeaderSize + 20

// WriterError struct check writer errors
type WriterError struct {
	errorString string
}

func (e *WriterError) Error() string {
	return "Writer error: " + e.errorString
}

// TODO: MAKE TESTS FOR WriteWav

// WriteWav function create a wav file.
func WriteWav(fw *AudioData, fileName string) error {

	//create file
	file, err := os.OpenFile(fileName, os.O_CREATE|os.O_TRUNC, 0666)
	if err != nil {
		return &WriterError{err.Error()}
	}

	defer file.Close()

	//create buffering stream
	writer := bufio.NewWriter(file)

	//create-write header
	bytesPerSample := int(fw.header.BitsPerSample / uint16(byteSize))
	dataSize := len(fw.OneChannelData) * int(fw.header.NumChannels) * bytesPerSample

	riffSize := dataSize + int(riffHeaderByteSize)

	writer.Write(getRiffID())
	writer.Write(uint32ToBytes(uint32(riffSize), int32Size))
	writer.Write(getWaveID())

	writer.Write(getFmtID())
	writer.Write(uint32ToBytes(fmtHeaderSize, int32Size))
	writer.Write(uint32ToBytes(uint32(fw.header.AudioFormat), int16Size))
	writer.Write(uint32ToBytes(uint32(fw.header.NumChannels), int16Size))
	writer.Write(uint32ToBytes(fw.header.SampleRate, int32Size))
	writer.Write(uint32ToBytes(fw.header.ByteRate, int32Size))
	writer.Write(uint32ToBytes(uint32(fw.header.BlockAlign), int16Size))
	writer.Write(uint32ToBytes(uint32(fw.header.BitsPerSample), int16Size))

	writer.Write(getDataID())
	writer.Write(uint32ToBytes(uint32(dataSize), int32Size))

	//write data
	for i := 0; i < len(fw.OneChannelData); i++ {
		writer.Write(uint32ToBytes(fw.OneChannelData[i], uint(bytesPerSample)))

		if fw.GetHeader().NumChannels == 2 {
			writer.Write(uint32ToBytes(fw.TwoChannelData[i], uint(bytesPerSample)))
		}
	}
	writer.Flush()

	return nil
}

//TODO: test this function
//uint32ToBytes creates bytes slice from uint32 number
func uint32ToBytes(number uint32, size uint) []byte {

	const maxSize = 4
	bytesData := make([]byte, maxSize)
	binary.LittleEndian.PutUint32(bytesData, number)

	return bytesData[:size]
}
